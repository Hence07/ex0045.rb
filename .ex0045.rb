require "./walking_on_monsta_street.rb"
require "./bienbenido_at_monstapalace.rb"
require "./main_entrance.rb"
require "./ghost_room.rb"
require "./vodoo_room.rb"
require "./dead.rb"
require "./prompt.rb"

class Game
    def initialize(start)
        @quips = [
          "Game Over!",
          "Pull up!",
          "Best oF you!",
          "End Of The Road!"
        ]
        @start = start
        puts "in init @ start = " + @start.inspect
    end

    def play()
      puts "@start => " + @start.inspect
      next_room = @start
      while true
        puts "\n---------"
        room = method(next_room)
        next_room = room.call()
      end
    end

    def start()
      puts <<MESSAGE
There is a haunted house up the road.
There is believed to be an vodoo of great value in the house...
But will you risk life and limb to get it?
Will you go and explore the house?
MESSAGE

      prompt()
      action = gets.chomp()

      if action == "yes"
        return :walking_on_monsta_street
      elsif action == "no"
        puts "Ah, well, maybe you'll have the balls to play someday."
        Process.exit()
      else
        return :dead
      end
    end
end

a_game = Game.new(:start)
a_game.play()
